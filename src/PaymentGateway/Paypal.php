<?php

namespace PaymentGateway;


class Paypal extends Gateway {

    public function __construct()
    {
        parent::__construct();

        $this->defaultCurrency = 'EUR';
        $this->exchangeRate    = 1.08;
        $this->enabled         = true;
    }

    public function pay(Array $charge)
    {
        $charge = parent::pay($charge);

        return [
            'success' => true,
            'charge'  => $charge,
        ];
    }
}