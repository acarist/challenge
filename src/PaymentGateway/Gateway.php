<?php

namespace PaymentGateway;

use Exception;
use App;
use Slim\Http\Response;

abstract class Gateway {

    protected $defaultCurrency;
    protected $exchangeRate;
    protected $enabled;

    protected static $instances = [];
    protected static $container;

    public function __construct()
    {
        // Maybe some preparation
    }

    public static function singleton($gatewayName)
    {
        if ( ! static::$instances[ $gatewayName ]) {
            $ns    = __NAMESPACE__;
            $class = $ns . '\\' . ucfirst($gatewayName);
            if (class_exists($class)) {
                static::$instances[ $gatewayName ] = new $class;
            } else {
                throw new Exception(sprintf('Gateway "%s" not found', $gatewayName));
            }
        }

        return static::$instances[ $gatewayName ];
    }


    public function checkCurrency(Array $charge)
    {
        if ($charge['currency'] === $this->defaultCurrency) {
            $charge['exchangeRateApplied'] = false;
            $charge['chargedAmount']       = $charge['amount'];

            return $charge;
        }

        // Before charging, exchange rate should be applied to the amount
        $charge['exchangeRateApplied'] = true;
        $charge['chargedAmount']       = $charge['amount'] * $this->exchangeRate;

        return $charge;
    }

    public function pay(Array $charge)
    {
        if ( ! $this->enabled) {
            throw new Exception('Payment gateway is not available at the moment');
        }

        // TODO: Verify $charge array

        return $this->checkCurrency($charge);
    }

    public function sendVoucher($charge)
    {
        $container = App::getContainer();
        $response  = new Response;
        $now       = new \DateTime();

        $container['view']->render($response, 'payment-mail.php', [
            'charge' => $charge,
            'time'   => $now->format('d-m-Y H:i:s'),
        ]);

        $html = $response->getBody();

        $to        = 'accounting@testtest.com';
        $subject   = 'Payment successful';
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        mail($to, $subject, $html, implode("\r\n", $headers));
    }

    public static function getGateways()
    {

        // Gather list of gateways somehow
        return [
            [
                'name'  => 'paypal',
                'label' => 'Paypal',
            ],
            [
                'name'  => 'payu',
                'label' => 'PayU',
            ],
            [
                'name'  => 'paytrek',
                'label' => 'Paytrek',
            ],
        ];
    }

}