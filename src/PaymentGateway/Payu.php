<?php

namespace PaymentGateway;


class Payu extends Gateway {

    public function __construct()
    {
        parent::__construct();

        $this->defaultCurrency = 'TRY';
        $this->exchangeRate    = 1.12;
        $this->enabled         = true;
    }

    public function pay(Array $charge)
    {
        $charge = parent::pay($charge);

        return [
            'success' => true,
            'charge'  => $charge,
        ];
    }
}