<?php

namespace PaymentGateway;


class Paytrek extends Gateway {

    public function __construct()
    {
        parent::__construct();

        $this->defaultCurrency = 'USD';
        $this->exchangeRate    = 1.10;
        $this->enabled         = true;
    }

    public function pay(Array $charge)
    {
        $charge = parent::pay($charge);

        return [
            'success' => true,
            'charge'  => $charge,
        ];
    }
}