<?php
namespace App\Controller;


use PaymentGateway\Gateway;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends Controller {

    // Form
    public function getIndexAction(Request $request, Response $response, $args)
    {
        return $this->view->render($response, 'payment-form.php', [
            // List of gateways
            'gateways' => Gateway::getGateways(),
        ]);
    }

    // Pay
    public function postIndexAction(Request $request, Response $response, $args)
    {
        // TODO: Check session for ongoing payment process to prevent duplicate requests

        $customerName = $request->getParsedBodyParam('name');
        $gatewayName  = $request->getParsedBodyParam('gateway');
        $amount       = $request->getParsedBodyParam('value');
        $currency     = $request->getParsedBodyParam('currency');

        // TODO: Validate user input

        // TODO: Set session values regarding current payment process

        $gateway  = Gateway::singleton($gatewayName);
        $charge   = [
            'currency' => $currency,
            'amount'   => $amount,
            'name'     => $customerName,
            'gateway'  => $gatewayName,
        ];
        $result   = $gateway->pay($charge);
        $gateway->sendVoucher($result['charge']);

        return $response->withJson($result);
    }

}