<?php
namespace App\Controller;

use Interop\Container\ContainerInterface;

abstract class Controller {

    /**
     * @var $container ContainerInterface
     */
    protected $container;

    /**
     * @var $view \Slim\Views\PhpRenderer
     */
    protected $view;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view      = $this->container['view'];
    }

}