<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title ?></title>
</head>
<body>

<h1>Payment Page</h1>

<form action="." method="post">

    <div>
        <label for="name">name:</label>
        <input id="name" name="name" type="text" />
    </div>

    <div>
        <label for="gateway">payment gateway:</label>
        <select name="gateway" id="gateway">
            <?php foreach($gateways as $gateway): ?>
            <option value="<?php echo $gateway['name'] ?>"><?php echo $gateway['label'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div>
        <label for="value">value:</label>
        <input id="value" name="value" type="text" />
        <select id="currency" name="currency">
            <option value="EUR">EUR</option>
            <option value="USD">USD</option>
            <option value="TRY">TRY</option>
        </select>
        <button type="submit">pay</button>
    </div>

</form>

</body>
</html>