<?php
use App\Controller\HomeController;
use Slim\Views\PhpRenderer as View;

class App {

    protected static $container;

    public static function getContainer()
    {
        return static::$container;
    }

    public function __construct()
    {
        $config = [
            'settings' => [
                'displayErrorDetails' => true,
            ],
        ];

        $app               = new \Slim\App($config);
        $container         = $app->getContainer();
        static::$container = $container;

        $container['view'] = new View(__DIR__ . '/App/views/');

        $app->get('/', HomeController::class . ':getIndexAction');
        $app->post('/', HomeController::class . ':postIndexAction');

        $app->run();
    }
}

