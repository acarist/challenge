# Payment Challenge
### Kurulum

Kurulum için bir env ya da db ayarı olmadığından paketler kurulup serve edilebilir.

```sh
$ composer install
$ php -S 0.0.0.0:8080 -t public/index.php
```

## Projede Kullanılan Elementler
  - Slim Framework
  - Slim PHP View Renderer

## Klasör Yapısı:
```
├── composer.json
├── composer.lock
├── public
│   └── index.php
└── src
    ├── App
    │   ├── Controller
    │   │   ├── Controller.php
    │   │   └── HomeController.php
    │   └── views
    │       ├── payment-form.php
    │       └── payment-mail.php
    ├── App.php
    ├── PaymentGateway
    │   ├── Gateway.php
    │   ├── Paypal.php
    │   ├── Paytrek.php
    │   └── Payu.php
    └── boot.php
```

## Slim Boot Logic 

*public/index.php* içerisinden *src/boot.php* require ediliyor. *boot.php* 'de ise App klasöründe bulunan App.php içerisindeki App sınıfı init ediliyor. Bu sınıfın görevi dependecy container'ı çevrelemek ve route belirlemek.

Route'lar *Controller* klasörü altındaki sınıflara yönlendirilmiştir. Buradaki bütün controller'lar "Controller.php" temel sınıfından türetiliyor. Bizim sınıfımız *HomeController* sınıfı. Bu sınıfta 2 method var : **getIndexAction, postIndexAction**. İsimlerinden anlaşılacağı gibi bu methodlardan biri form sayfasını render ediyor, diğeri ise post verilerini karşılayıp ödeme işlemini başlatıyor.

views klasöründe 2 adet view bulunuyor. Bunlardan "payment-form.php" ödeme formu view'ı "payment-mail.php" ise *sendVoucher()* ile gönderilecek mail gövdesi view'ı.

## Payment Logic
Payment sınıfları *PaymentGateway* klasörü içinde yer alıyor. Burada **Gateway.php** içerisinde temel **Gateway** sınıfı bulunuyor. Ödeme yöntemleri bu sınıftan türetiliyor. 

Controller içerisinden seçilen ödeme yöntemine ait sınıf instantiate edilip *pay()* metodu çağrılıyor. Temel *Gateway* sınıfı doğrudan instantiate edilmediği için, bu sınfın *pay()* methodu aslında ödeme yönteminin içerisindeki *pay()* metodundan çağrılıyor. (*"parent::pay()"*) **Gateway** sınıfında bulunan *pay()* charge verilerini alarak *checkCurrency()* ile kur farkı kontrolü ve eklemesi yapıyor. Cevap olarak "charge" array'ini dönüyor. Bu da ekrana JSON olarak basılıyor.

##### Örnek Charge JSON

```
{"success":true,"charge":{"currency":"EUR","amount":"100","name":"test test","gateway":"paypal","exchangeRateApplied":false,"chargedAmount":100}}
```
Son olarak postIndexAction içerisinden *sendVoucher()* metodu çağrılarak mail gönderiliyor.

## Dipnotlar
* Temel Gateway ve Controller sınıfı doğrudan instantiate edilmemesi için abstract olarak belirlenmiştir.
* Ödeme yöntemlerine ait varsayılan kur, kur farkı ve aktif olu olmadığı bilgisi, ödeme yöntemi sınıfı içerisinden değiştirilebilir.
* Ödeme yöntemleri Gateway sınıfı içerisinden *getGateways()* içerisinde array olarak tanımlanmıştır. 
* **src/** klasörü *psr-0* olarak autoload'a eklenmiştir.

## TLDR
* Slim ayağa kalkar
* Form render edilir.
* Post edilince ilgili ödeme yöntemi çağrılır.
* temel Gateway içerisinde kur hesaplamaları yapılır.
* Kur bilgisi ve çekilen tutar geri döner.